const DSNV = "DSNV";
var dsnv = [];

function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", jsonDsnv);
}

//lấy dữ liệu từ LocalStorage khi user load trang
var dataJson = localStorage.getItem("DSNV");
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  //convert data vì dữ liệu không có method
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luong,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv);
}

function themNv() {
  var nv = layThongTinTuForm();

  var isValid = kiemTraValidate(nv);

  if (isValid == 8) {
    // add nhân viên vào array
    dsnv.push(nv);
    //Lưu vào LocalStorage
    luuLocalStorage();

    // render ra danh sách
    renderDsnv(dsnv);
  }
}

// thêm function xóa Nv
function xoaNv(id) {
  console.log("id: ", id);
  // splice
  var viTri = timKiemViTri(id, dsnv);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    // lưu lại vào Local Storage
    luuLocalStorage();
    // render lại giao diện sau khi xóa
    renderDsnv(dsnv);
  }
}

// Thêm function sửa Nhân viên
function suaNv(id) {
  var viTri = timKiemViTri(id, dsnv);
  console.log(" viTri: ", viTri);
  if (viTri == -1) return;

  var data = dsnv[viTri];
  console.log(" data: ", data);
  showThongTinLenForm(data);

  //Ngăn cản user chỉnh sửa tài khoản cá nhân
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
}

function capNhatNv() {
  var data = layThongTinTuForm();

  var isValid = kiemTraCapNhat(data);
  if (isValid == 7) {
    var viTri = timKiemViTri(data.taiKhoan, dsnv);
    if (viTri == -1) return;

    dsnv[viTri] = data;
    renderDsnv(dsnv);
    luuLocalStorage();
  }

  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
}
