function kiemTraTrung(id, dsnv, idErr, message) {
  let index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}

// Validate Tài Khoản
function kiemTraTaiKhoan(value, idErr, message) {
  //Tối đa 4-6 kí số, bao gồm chữ và số
  var reg = /^[a-z | 0-9]{4,6}$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate tên nhân viên
function kiemTraHoTen(value, idErr, message) {
  //Là chữ và có thể nhập dc tiếng việt
  var reg =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate Email
function kiemTraEmail(value, idErr, message) {
  let reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate Password
function kiemTraPass(value, idErr, message) {
  var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;

  let isPass = reg.test(value);
  if (isPass) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate Ngày Làm
function kiemTraNgayLam(value, idErr, message) {
  var reg = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate Lương Cơ Bản
function kiemTraLuong(value, idErr, message) {
  var reg = /^([1-9]\d{6,6}|1\d{7}|20000000)$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate Chức Vụ
function kiemTraChucVu(value, idErr, message) {
  var reg = /^Giám đốc|Trưởng phòng|Nhân viên$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

// Validate giờ làm
function kiemTraGioLam(value, idErr, message) {
  var reg = /^(200|19[0-9]|[8-9][0-9]|1[0-9][0-9])$/;
  let isCorrect = reg.test(value);
  if (isCorrect) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
