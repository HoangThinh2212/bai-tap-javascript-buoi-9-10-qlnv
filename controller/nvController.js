function layThongTinTuForm() {
  // Lấy dữ liệu từ form
  var taiKhoanNv = document.getElementById("tknv").value;
  var hoTenNv = document.getElementById("name").value;
  var emailNv = document.getElementById("email").value;
  var matKhauNv = document.getElementById("password").value;
  var ngayLamNv = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value * 1;
  var chucVuNv = document.getElementById("chucvu").value;
  var gioLamNv = document.getElementById("gioLam").value;
  //tạo nhân viên
  var nv = new NhanVien(
    taiKhoanNv,
    hoTenNv,
    emailNv,
    matKhauNv,
    ngayLamNv,
    luongCoBan,
    chucVuNv,
    gioLamNv
  );
  return nv;
}

function renderDsnv(dsnv) {
  var contentHTML = "";
  for (var index = 0; index < dsnv.length; index++) {
    var currentNv = dsnv[index];

    var contentTr = `<tr>
      <td>${currentNv.taiKhoan}</td>
      <td>${currentNv.hoTen}</td>
      <td>${currentNv.email}</td>
      <td>${currentNv.ngayLam}</td>
      <td>${currentNv.chucVu}</td>
      <td>${currentNv.tongLuong()}</td>
      <td>${currentNv.tongGioLam()}</td>
      <button onclick="xoaNv('${
        currentNv.taiKhoan
      }')" class="btn btn-danger col-*">Xóa</button>
      <button onclick="suaNv('${
        currentNv.taiKhoan
      }')"  class="btn btn-warning col-*"data-toggle="modal"
      data-target="#myModal">Sửa</button>
  
      </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var item = dsnv[index];
    // Nếu tìm thấy thì dừng function và trả về index hiện tại
    if (item.taiKhoan == id) {
      return index;
    }
  }
  // Tự quy định nếu ko tìm thấy thì trả về -1
  return -1;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}

function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}
// Hàm Kiểm Tra các validate trước khi thêm nhân viên
function kiemTraValidate(nv) {
  let count = 0;
  //Kiểm tra tài khoản
  if (
    kiemTraRong(
      nv.taiKhoan,
      "tbTKNV",
      "Tài Khoản Nhân Viên không được để trống"
    ) &&
    kiemTraTrung(
      nv.taiKhoan,
      dsnv,
      "tbTKNV",
      "Tài Khoản Nhân Viên đã tồn tại"
    ) &&
    kiemTraTaiKhoan(
      nv.taiKhoan,
      "tbTKNV",
      "Tài Khoản Nhân Viên chỉ chứa kí tự, số lượng tối thiểu là 4 và tối đa là 6"
    )
  ) {
    count++;
  }

  // Kiểm tra Họ và tên
  if (
    kiemTraRong(nv.hoTen, "tbTen", "Tên Nhân Viên không được để trống") &&
    kiemTraHoTen(nv.hoTen, "tbTen", "Tên Nhân Viên phải là chữ")
  ) {
    count++;
  }

  // Kiểm tra Email
  if (
    kiemTraRong(nv.email, "tbEmail", "Email Nhân Viên không được để trống") &&
    kiemTraEmail(nv.email, "tbEmail", "Email không đúng định dạng")
  ) {
    count++;
  }

  // Kiểm tra Password
  if (
    kiemTraRong(
      nv.matKhau,
      "tbMatKhau",
      "Mật Khẩu Nhân Viên không được để trống"
    ) &&
    kiemTraPass(
      nv.matKhau,
      "tbMatKhau",
      "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    )
  ) {
    count++;
  }

  // Kiểm tra ngày làm
  if (
    kiemTraRong(nv.ngayLam, "tbNgay", "Ngày Làm không được để trống") &&
    kiemTraNgayLam(
      nv.ngayLam,
      "tbNgay",
      "Ngày Làm không đúng định dạng mm/dd/yyyy"
    )
  ) {
    count++;
  }

  // Kiểm tra Lương cơ bản
  if (
    kiemTraRong(nv.luong, "tbLuongCB", "Lương không được để trống") &&
    kiemTraLuong(
      nv.luong,
      "tbLuongCB",
      "Lương cơ bản 1 000 000 - 20 000 000 và không chứa dấu . ,"
    )
  ) {
    count++;
  }

  // Kiểm tra chức vụ
  if (kiemTraChucVu(nv.chucVu, "tbChucVu", "Mời chọn chức vụ cho Nhân viên")) {
    count++;
  }

  // Kiểm tra giờ làm
  if (
    kiemTraRong(nv.gioLam, "tbGiolam", "Giờ Làm không được để trống") &&
    kiemTraGioLam(nv.gioLam, "tbGiolam", "Số Giờ Làm trong tháng 80 - 200 giờ")
  ) {
    count++;
  }
  console.log("count: ", count);
  return count;
}
// Hàm Kiểm Tra các validate trước khi Cập nhật nhân viên

function kiemTraCapNhat(nv)
{
  let count = 0;
  // Kiểm tra Họ và tên
  if (
    kiemTraRong(nv.hoTen, "tbTen", "Tên Nhân Viên không được để trống") &&
    kiemTraHoTen(nv.hoTen, "tbTen", "Tên Nhân Viên phải là chữ")
  ) {
    count++;
  }

  // Kiểm tra Email
  if (
    kiemTraRong(nv.email, "tbEmail", "Email Nhân Viên không được để trống") &&
    kiemTraEmail(nv.email, "tbEmail", "Email không đúng định dạng")
  ) {
    count++;
  }

  // Kiểm tra Password
  if (
    kiemTraRong(
      nv.matKhau,
      "tbMatKhau",
      "Mật Khẩu Nhân Viên không được để trống"
    ) &&
    kiemTraPass(
      nv.matKhau,
      "tbMatKhau",
      "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    )
  ) {
    count++;
  }

  // Kiểm tra ngày làm
  if (
    kiemTraRong(nv.ngayLam, "tbNgay", "Ngày Làm không được để trống") &&
    kiemTraNgayLam(
      nv.ngayLam,
      "tbNgay",
      "Ngày Làm không đúng định dạng mm/dd/yyyy"
    )
  ) {
    count++;
  }

  // Kiểm tra Lương cơ bản
  if (
    kiemTraRong(nv.luong, "tbLuongCB", "Lương không được để trống") &&
    kiemTraLuong(
      nv.luong,
      "tbLuongCB",
      "Lương cơ bản 1 000 000 - 20 000 000 và không chứa dấu . ,"
    )
  ) {
    count++;
  }

  // Kiểm tra chức vụ
  if (kiemTraChucVu(nv.chucVu, "tbChucVu", "Mời chọn chức vụ cho Nhân viên")) {
    count++;
  }

  // Kiểm tra giờ làm
  if (
    kiemTraRong(nv.gioLam, "tbGiolam", "Giờ Làm không được để trống") &&
    kiemTraGioLam(nv.gioLam, "tbGiolam", "Số Giờ Làm trong tháng 80 - 200 giờ")
  ) {
    count++;
  }
  console.log("count: ", count);
  return count;
}