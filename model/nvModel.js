function NhanVien(
  taiKhoanNv,
  hoTenNv,
  emailNv,
  matKhauNv,
  ngayLamNv,
  luongCoBan,
  chucVuNv,
  gioLamNv
) {
  this.taiKhoan = taiKhoanNv;
  this.hoTen = hoTenNv;
  this.email = emailNv;
  this.matKhau = matKhauNv;
  this.ngayLam = ngayLamNv;
  this.luong = luongCoBan;
  this.chucVu = chucVuNv;
  this.gioLam = gioLamNv;
  this.tongLuong = function () {
    var tongLuong = 0;
    chucVuDcChon = document.getElementById("chucvu").value;
    if (chucVuDcChon == "Sếp") {
      tongLuong = this.luong * 3;
      return (tongLuong = tongLuong.toLocaleString());
    } else if (chucVuDcChon == "Trưởng phòng") {
      tongLuong = this.luong * 2;
      return (tongLuong = tongLuong.toLocaleString());
    } else if (chucVuDcChon == "Nhân viên") {
      tongLuong = this.luong * 1;
      return (tongLuong = tongLuong.toLocaleString());
    }
  };
  this.tongGioLam = function () {
    var loaiNv = "";
    var gioLamNhap = document.getElementById("gioLam").value * 1;
    if (gioLamNhap < 192) {
      return (loaiNv = "Giỏi");
    } else if (gioLamNhap < 176) {
      return (loaiNv = "Khá");
    } else if (gioLamNhap < 160) {
      return (loaiNv = "Trung Bình");
    } else return (loaiNv = "Xuất sắc");
  };
}
